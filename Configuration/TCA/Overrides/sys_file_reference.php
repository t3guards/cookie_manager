<?php
$customColumns = [
    'youtube_id' => [
        'label' => 'VIDEO-ID',
        'config' => [
            'type' => 'input',
            'size' => 100,
            'default' => null
        ],
    ],
    'iframe_manager' => [
        'exclude' => true,
        'label' => 'IFrame-Manager',
        'config' => [
            'type' => 'check',
            'renderType' => 'checkboxToggle',
            'default' => 0,
            'items' => [
                [
                    0 => '',
                    1 => '',
                ]
            ],
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'sys_file_reference',
    $customColumns
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'sys_file_reference',
    'videoOverlayPalette',
    'youtube_id, iframe_manager'
);
