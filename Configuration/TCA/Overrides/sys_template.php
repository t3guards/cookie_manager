<?php
defined('TYPO3') || die();

call_user_func(function() {

    $extensionKey = 'cookie_manager';

    // If automatically include of TypoScript is disabled, then you can include it in the (BE) static-template select-box
    if ($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extensionKey]['config']['typoScript'] === '0') {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            $extensionKey,
            'Configuration/TypoScript',
            'T3 Guards - Cookieconsernt and Iframemanager'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            $extensionKey,
            'Configuration/TypoScript/addFluid.typoscript',
            'T3 Guards - Cookieconsernt and Iframemanager'
        );
    }
});
