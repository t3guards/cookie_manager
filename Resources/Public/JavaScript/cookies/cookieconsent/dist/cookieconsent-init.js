// obtain iframemanager object
var manager = iframemanager();

// obtain cookieconsent plugin
var cc = initCookieConsent();

// Configure with youtube embed
// Configure with youtube embed
manager.run({
    currLang: 'de',
    services : {
        youtube : {
            embedUrl: 'https://www.youtube-nocookie.com/embed/{data-id}',
            thumbnailUrl: 'https://i3.ytimg.com/vi/{data-id}/hqdefault.jpg',
            iframe : {
                allow : 'accelerometer; encrypted-media; gyroscope; picture-in-picture; fullscreen;',
            },
            cookie : {
                name : 'cc_youtube'
            },
            languages : {
                de : {
                    notice: 'This content is hosted by a third party. By showing the external content you accept the <a rel="noreferrer" href="https://www.youtube.com/t/terms" title="Terms and conditions" target="_blank">terms and conditions</a> of youtube.com.',
                    loadBtn: 'Load video',
                    loadAllBtn: 'Don\'t ask again'
                }
            }
        },
        GoogleMaps: {
            embedUrl: 'https://www.google.com/maps/embed/v1/place?key=AIzaSyBzRGsJGMLqAZ3SML_A3UH-__9ZT78S65E&q={data-id}',
            iframe: {
                allow : 'picture-in-picture; fullscreen;'
            },
            cookie: {
                name: 'cc_maps'
            },
            languages: {
                'de' : {
                    notice: 'Google Maps ...',
                    loadBtn: 'Load map',
                    loadAllBtn: 'Don\'t ask again'
                }
            }
        }
    }
});

// run plugin with config object
cc.run({
    current_lang: 'en',
    autorun: true,
    auto_language: 'document',
    theme_css: '/typo3conf/ext/cogtail_publishing/Resources/Public/JavaScript/cookies/cookieconsent/dist/cookieconsent.css',
    autoclear_cookies: true,
    page_scripts: true,
    force_consent: false,
    onAccept: function(){
        console.log('onAccept fired!')

        // If analytics category is disabled => load all iframes automatically
        if(cc.allowedCategory('media')){
            console.log('iframemanager: loading all iframes');
            manager.acceptService('all');
        }
    },

    onChange: function(cookie, changed_preferences){
      if(!cc.allowedCategory('analytics')){
        typeof gtag === 'function' && gtag('consent', 'update', {
        'analytics_storage': 'denied'
        });
      }
      if(!cc.allowedCategory('media')){
        manager.rejectService('all');
      }
    },
    gui_options: {
        consent_modal: {
          layout: 'cloud',
          position: 'bottom center'
     }
    },
    languages: {
        'en': {
            consent_modal: {
                title: 'Your privacy is important to us!',
                description: 'This website uses essential cookies to ensure its proper operation and tracking cookies to understand how you interact with it. The latter will be set only upon approval.',
                primary_btn: {
                    text: 'Accept',
                    role: 'accept_all'              // 'accept_selected' or 'accept_all'
                },
                secondary_btn: {
                    text: 'Settings',
                    role: 'settings'                // 'settings' or 'accept_necessary'
                }
            },
            settings_modal: {
                title: 'Cookie preferences',
                save_settings_btn: 'Save settings',
                accept_all_btn: 'Accept all',
                reject_all_btn: 'Reject all',       // optional, [v.2.5.0 +]
                cookie_table_headers: [
                    {col1: 'Name'},
                    {col2: 'Domain'},
                    {col3: 'Expiration'},
                    {col4: 'Description'},
                    {col5: 'Type'}
                ],
                blocks: [
                    {
                        title: 'Cookie usage',
                        description: 'I use cookies to ensure the basic functionalities of the website and to enhance your online experience. You can choose for each category to opt-in/out whenever you want.'
                    }, {
                        title: 'Strictly necessary cookies',
                        description: 'These cookies are essential for the proper functioning of my website. Without these cookies, the website would not work properly.',
                        toggle: {
                            value: 'necessary',
                            enabled: true,
                            readonly: true
                        }
                    },
                    {
                        title: 'Analytics',
                        description: 'These cookies collect information about how you use the website, which pages you visited and which links you clicked on. All of the data is anonymized and cannot be used to identify you.',
                        toggle: {
                            value: 'analytics',
                            enabled: false,
                            readonly: false
                        },
                        cookie_table: [
                            {
                                col1: '_ga',
                                col2: 'google.com',
                                col3: '2 years',
                                col4: 'Google Analytics',
                                col5: 'Permanent cookie',
                                is_regex: true,
                                domain: 't3guards.de'
                            },
                            {
                                col1: '_gid',
                                col2: 'google.com',
                                col3: '1 day',
                                col4: 'Google Analytics',
                                col5: 'Permanent cookie',
                                domain: 't3guards.de'
                            }
                        ]
                        },
                        {
                            title: 'Media',
                            description: 'Aktivieren Sie die Nutzung von Medien.',
                            toggle: {
                                value: 'media',
                                enabled: false,
                                readonly: false
                            },
                            cookie_table: [
                                {
                                    col1: 'cc_youtube',
                                    col2: 't3guards.de',
                                    col3: '1 day',
                                    col4: 'YouTube',
                                    col5: 'Permanent cookie',
                                    domain: 'b5.t3guards.de'
                                }
                            ]
                        },
                        {
                            //  title: 'Marketing',
                            // ...
                        },
                        {
                        title: 'More information',
                        description: 'If you have any questions about cookies and their use, please use the contact options on this website.',
                    }
                ]
            }
        },
        'de-DE': {
            consent_modal: {
                title: 'Der Schutz Ihrer Daten ist uns wichtig! ',
                description: 'Wir verwenden Cookies und Third-Party-Tools, um die Leistung der Website zu verbessern, Analysen durchzuführen und Ihnen Inhalte bereitzustellen, die für Sie relevant sind.',
                primary_btn: {
                    text: 'Okay',
                    role: 'accept_all'              // 'accept_selected' or 'accept_all'
                },
                secondary_btn: {
                    text: 'Einstellungen',
                    role: 'settings'                // 'settings' or 'accept_necessary'
                }
            },
            settings_modal: {
                title: 'Einstellungen',
                save_settings_btn: 'Speichern',
                accept_all_btn: 'OK, weiter',
                reject_all_btn: 'Alle ablehnen',       // optional, [v.2.5.0 +]
                cookie_table_headers: [
                    {col1: 'Name'},
                    {col2: 'Domain'},
                    {col3: 'Ablauf'},
                    {col4: 'Beschreibung'},
                    {col5: 'Typ'}
                ],
                blocks: [
                    {
                        title: 'Der Schutz Ihrer Daten ist uns wichtig!',
                        description: 'Wir verwenden Cookies und Third-Party-Tools, um die Leistung der Website zu verbessern, Analysen durchzuführen und Ihnen Inhalte bereitzustellen, die für Sie relevant sind.'
                    }, {
                        title: 'Technisch erforderlich',
                        description: 'Diese Cookies sind für das ordnungsgemäße Funktionieren dieser Website unerlässlich. Ohne diese Cookies würde die Website nicht richtig funktionieren.',
                        toggle: {
                            value: 'necessary',
                            enabled: true,
                            readonly: true
                        }
                    },
                    {
                        title: 'Analytics',
                        description: 'Diese Cookies sammeln Informationen darüber, wie Sie die Website nutzen, welche Seiten Sie besucht und welche Links Sie angeklickt haben. Alle Daten sind anonymisiert und können nicht verwendet werden, um Sie zu identifizieren.',
                        toggle: {
                            value: 'analytics',
                            enabled: false,
                            readonly: false
                        },
                        cookie_table: [
                            {
                                col1: '^_ga',
                                col2: 'google.com',
                                col3: '2 years',
                                col4: 'Google Analytics',
                                col5: 'Permanent cookie',
                                is_regex: true,
                                domain: 't3guards.de'
                            },
                            {
                                col1: '_gid',
                                col2: 'google.com',
                                col3: '1 day',
                                col4: 'Google Analytics',
                                col5: 'Permanent cookie',
                                domain: 't3guards.de'
                            }
                        ]
                    },
                    {
                        title: 'Medien',
                        description: 'Aktivieren Sie die Nutzung von Medien.',
                        toggle: {
                            value: 'media',
                            enabled: false,
                            readonly: false
                        },
                        cookie_table: [
                            {
                                col1: 'cc_youtube',
                                col2: 't3guards.de',
                                col3: '1 day',
                                col4: 'YouTube',
                                col5: 'Permanent cookie',
                                domain: 'b5.t3guards.de'
                            }
                        ]
                    },
                    {
                        //  title: 'Marketing',
                        // ...
                    },
                    {
                        title: 'Weitere Information',
                        description: 'Bei Fragen zu Cookies und Ihrer Verwendung nutzen Sie bitte die Kontaktmöglichkeiten auf dieser Website.',
                    }
                ]
            }
        },
        'de': {
            consent_modal: {
                title: 'Der Schutz Ihrer Daten ist uns wichtig! ',
                description: 'Wir verwenden Cookies und Third-Party-Tools, um die Leistung der Website zu verbessern, Analysen durchzuführen und Ihnen Inhalte bereitzustellen, die für Sie relevant sind.',
                primary_btn: {
                    text: 'Okay',
                    role: 'accept_all'              // 'accept_selected' or 'accept_all'
                },
                secondary_btn: {
                    text: 'Einstellungen',
                    role: 'settings'                // 'settings' or 'accept_necessary'
                }
            },
            settings_modal: {
                title: 'Einstellungen',
                save_settings_btn: 'Speichern',
                accept_all_btn: 'OK, weiter',
                reject_all_btn: 'Alle ablehnen',       // optional, [v.2.5.0 +]
                cookie_table_headers: [
                    {col1: 'Name'},
                    {col2: 'Domain'},
                    {col3: 'Ablauf'},
                    {col4: 'Beschreibung'},
                    {col5: 'Typ'}
                ],
                blocks: [
                    {
                        title: 'Der Schutz Ihrer Daten ist uns wichtig!',
                        description: 'Wir verwenden Cookies und Third-Party-Tools, um die Leistung der Website zu verbessern, Analysen durchzuführen und Ihnen Inhalte bereitzustellen, die für Sie relevant sind.'
                    }, {
                        title: 'Technisch erforderlich',
                        description: 'Diese Cookies sind für das ordnungsgemäße Funktionieren dieser Website unerlässlich. Ohne diese Cookies würde die Website nicht richtig funktionieren.',
                        toggle: {
                            value: 'necessary',
                            enabled: true,
                            readonly: true
                        }
                    },
                    {
                        title: 'Analytics',
                        description: 'Diese Cookies sammeln Informationen darüber, wie Sie die Website nutzen, welche Seiten Sie besucht und welche Links Sie angeklickt haben. Alle Daten sind anonymisiert und können nicht verwendet werden, um Sie zu identifizieren.',
                        toggle: {
                            value: 'analytics',
                            enabled: false,
                            readonly: false
                        },
                        cookie_table: [
                            {
                                col1: '^_ga',
                                col2: 'google.com',
                                col3: '2 years',
                                col4: 'Google Analytics',
                                col5: 'Permanent cookie',
                                domain: 't3guards.de',
                                is_regex: true
                            },
                            {
                                col1: '_gid',
                                col2: 'google.com',
                                col3: '1 day',
                                col4: 'Google Analytics',
                                col5: 'Permanent cookie',
                                domain: 't3guards.de'
                            },
                            {
                                col1: '_gid',
                                col2: 'google.com',
                                col3: '1 day',
                                col4: 'Google Analytics',
                                col5: 'Permanent cookie',
                                domain: 't3guards.de'
                            }
                        ]
                        },
                        {
                            title: 'Medien',
                            description: 'Aktivieren Sie die Nutzung von Medien.',
                            toggle: {
                                value: 'media',
                                enabled: false,
                                readonly: false
                            },
                            cookie_table: [
                                {
                                    col1: 'cc_youtube',
                                    col2: 't3guards.de',
                                    col3: '1 day',
                                    col4: 'YouTube',
                                    col5: 'Permanent cookie',
                                    domain: 'b5.t3guards.de'
                                }
                            ]
                        },
                        {
                            //  title: 'Marketing',
                            // ...
                    },
                    {
                        title: 'Weitere Information',
                        description: 'Bei Fragen zu Cookies und Ihrer Verwendung nutzen Sie bitte die Kontaktmöglichkeiten auf dieser Website.',
                    }
                ]
            }
        }
    }
});
