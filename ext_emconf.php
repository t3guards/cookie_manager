<?php

/*******************************************************************
 * Extension Manager/Repository config file for ext: "cookie_manager"
 ******************************************************************/

$EM_CONF['cookie_manager'] = [
    'title' => 'T3 Guards - Iframemanager, Cookieconsent, Video Extender',
    'description' => 'T3 Guards - Iframemanager, Cookieconsent, Video Extender',
    'category' => 'misc',
    'author' => 'Rene Gast',
    'author_email' => 'kontakt@t3guards.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '11.0',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.13-13.9.99'
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Cogtail\\CookieManager\\' => 'Classes'
        ],
    ],
];
